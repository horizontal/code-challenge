#![HIVE: Horizontal Integration View Environment](gulpfile.js/lib/helper/hive-logo.jpg)

The Horizontal Integration Code Challenge is an interactive development challenge intended to test your ability to work within our development environment while completing a series of coding requirements.

This project is built on top of a limited version of our **HIVE** ecosystem which includes a blend of popular frontend technologies along with a [Gulp](http://gulpjs.com/) build workflow for generating your project.

---

## Challenge Requirements 
1. Clone the Code Challenge Repository (See installation below) to your local machine.

2. In the "creative" folder you will find the "product-tiles" component presented in PDF and Sketch format.  (A free 30 trial of sketch can be downloaded for the purposes of this challenge)

3. Using the "product-tiles" creative:

	1. A "product-tiles" component will contain no more than 3 product tiles at any given time. Each individual tile should be loaded randomly from the data. There should be no duplicates displayed across the 3 tiles.
	
	2. Data for each product-tile should be loaded dynamically from ajax data. This ajax data has been provided at the following address: [http://localhost:3000/api](http://localhost:3000/api)  Please note that the URL to the ajax will only work if you have run the development task in your terminal window (See usage below).
	
	3. Please use the Nunjucks templating language to render the ajax data as HTML. Nunjucks has been preinstalled (as well as jQuery) and a limited demonstration of this functionality  can be found in main.js, otherwise, please refer to Nunjucks api documentation.
	
	4.  Please architect the component so that any number of "product-tiles" components can be added to the page at any given time.  (They do not need to interact with each other so if duplicates occur that is acceptable, however, they should not interfere with each other.)
	
	5. When complete, commit and push your updates back to your branch and send your contact a note that your work is ready to review.  

## Global Dependencies 
**HIVE** is a powerful tool that is built on top of other powerful tools.  In order for **HIVE** to work, you will need the following global dependencies:

1. **Node:** We recommend using [NVM](https://github.com/creationix/nvm) to manage your node versions. HIVE has been tested on Node `0.12.x` - `5.9.0`.  
> **Note:** As of this writing, HIVE is not currently supported on Node `6.x.x`

2. **Sass:** [Sass](http://sass-lang.com/) is an extension of CSS that adds variables, nested rules, mixins, inline imports, and more, all with a fully CSS-compatible syntax.
3. **Gulp:** [Gulp](http://gulpjs.com/) is a task/build runner for development teams intended to automate and enhance your workflow.


## Installation
#### Clone the HIVE repository:

```bash
git clone -b <branch> https://bitbucket.org/horizontal/code-challenge.git MyChallenge

// Please be sure to substitute <branch> with the proper 
// branch name that was sent with the URL to this rep

cd MyChallenge
```
> The **HIVE** repository is stored on the Horizontal Integration BitBucket [account](https://bitbucket.org/horizontal/hive).  You will need to replace [USERNAME] with your own username and then enter your password credentials.

#### Install your project dependencies:
```bash
npm install
```

## Usage
#### Build Development
This runs the default gulp task passing the <code>--env dev</code> argument, which will compile site assets uncompressed with source maps. [BrowserSync](http://www.browsersync.io/) will serve up files to `localhost:3000` and will stream live changes to the code and assets to all connected browsers. 

```bash
npm run development
```
Alternately, you can call the Gulp task directly:

```
gulp --env dev
```

### Why npm scripts? 
NPM scripts add ./node_modules/bin to the path when run, using the packages version installed with this project, rather than a globally installed ones. Never `npm install` and get into mis-matched version issues again. These scripts are defined in the `scripts` property of `package.json`.

## Installed Technologies
The following technologies have been included:

1. **[jQuery](https://jquery.com/):** jQuery makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers,
2. **[Nunjucks Templating](https://mozilla.github.io/nunjucks/):** A powerful templating language with block inheritance, auto-escaping, macros, asynchronous control, and more.

## Configuration
Directory and top level settings are conveniently exposed in `./hive.config.json`. Use this file to update paths to match the directory structure of your project, and to adjust task options.

All task configuration objects have `src` and `dest` directories specified. These are relative to `root.src` and `root.dest` respectively. Each configuration also has an extensions array. This is used for file watching, and file deleting/replacing.

**If there is a feature you do not wish to use on your project, simply delete the configuration, and the task will be skipped.**

## Asset Task Details
A `README.md` with details about each asset task are available in their respective folders in the `src` directory:

- [JavaScript](src/javascripts)
- [Stylesheets](src/stylesheets)
- [HTML](src/html)
- [Fonts](src/fonts)
- [Images](src/images)
- [Static Files (favicons, app icons, etc.)](src/static)

***