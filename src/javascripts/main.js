// Main.js

/**
 * App Constructor
 * 
 */

const App = function() {
  this.init();
};

/**
 * App Prototype
 * 
 */

App.prototype.init = function() {
  console.log('App Init');

  nunjucks.configure(App.defaults.templatesPath, {
    autoescape: true,
    watch: true
  });

  $(function(){
    const templ = nunjucks.render('hello-world.njx');
    const $main = $('main');

    $main.append(templ);
  });
};

/**
 * App Defaults
 * 
 */

App.defaults = {
  templatesPath: 'templates'
};

/**
 * App Init
 * 
 */

window.app = new App();